using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] ParticleSystem[] mainEngineParticle; 
    [SerializeField] float mainThrust = 1000;
    [SerializeField] float rotationThrust = 100;
    [SerializeField] AudioClip mainEngine;

    Rigidbody rb;
    AudioSource audoioSource;

    bool isAlive;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audoioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessThrust();
        ProcesssRotation();
    }

    void ProcessThrust()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            rb.AddRelativeForce(Vector3.up * mainThrust * Time.deltaTime);
            mainEngineParticle[0].Play();
            if (!audoioSource.isPlaying)
            {
                audoioSource.PlayOneShot(mainEngine);
            }
        }
        else
        {
            mainEngineParticle[0].Stop();
            audoioSource.Stop();
        }

    }

    void ProcesssRotation()
    {
        if (Input.GetKey(KeyCode.A))
        {

            if (!mainEngineParticle[2].isPlaying)
            {
                mainEngineParticle[2].Play();
            }
            ApplyRotation(rotationThrust);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            if (!mainEngineParticle[1].isPlaying)
            {
                mainEngineParticle[1].Play();
            }
            ApplyRotation(-rotationThrust);
        }
        else
        {
            mainEngineParticle[1].Stop();
            mainEngineParticle[2].Stop();
        }

    }

    private void ApplyRotation(float rotationThisFrame)
    {
        rb.freezeRotation = true;
        transform.Rotate(Vector3.forward * rotationThisFrame * Time.deltaTime);
        rb.freezeRotation = false;
    }
}
